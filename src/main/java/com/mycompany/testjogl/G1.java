/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.testjogl;

/**
 *
 * @author stanichs
 */
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.Animator;
import com.jogamp.opengl.util.gl2.GLUT;
import com.jogamp.opengl.glu.GLU;
public class G1 extends JFrame implements GLEventListener, MouseMotionListener {

    private static final long serialVersionUID = 7376825297884956163L;

    private float rotateX, rotateY;
    private int lastX, lastY;

    public G1() {
        super("G1");
        setSize(800,600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        GLProfile glProfile = GLProfile.getDefault();
        GLCapabilities glCapabilities = new GLCapabilities(glProfile);
        glCapabilities.setDoubleBuffered(true);
        GLCanvas glCanvas = new GLCanvas(glCapabilities);
        glCanvas.addGLEventListener(this);
        glCanvas.addMouseMotionListener(this);
        add(glCanvas);
        addMouseMotionListener(this);
        rotateX = 0f; rotateY = 0f;
        Animator a = new Animator(glCanvas);
        a.start();
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                G1 g1 = new G1();
                g1.setVisible(true);
            }
        });
    }

    @Override
    public void display(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();
        gl.glClearColor(0,0,0,0);
        gl.glClear( GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT );
        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glLoadIdentity();
       // gl.glOrtho(-1,1,-1,1,-2,2);
       gl.glFrustumf(-1, 1, -1, 1, 2, 100);
        (new GLU()).gluLookAt(-3,10,-20,0,0,0,0,1,0);
        gl.glMatrixMode(GL2.GL_MODELVIEW);
        gl.glLoadIdentity();
        gl.glRotatef(rotateX,0,1,0);
        gl.glRotatef(rotateY,1,0,0);
        
        gl.glColor3d(1,0,0);
        (new GLUT()).glutSolidTeapot(0.5);
        
        
          gl.glPushMatrix();
   gl.glColor3d(0,1,0);

     gl.glBegin(GL.GL_LINES);
    gl.glVertex3f(0,0,0);
    gl.glVertex3f(0,100,0);
    gl.glEnd();
    gl.glColor3d(0,0,1);

    gl.glBegin(GL.GL_LINES);
    gl.glVertex3f(0,0,0);
    gl.glVertex3f(100,0,0);
    gl.glEnd();
    gl.glColor3d(1,1,1);

    gl.glBegin(GL.GL_LINES);
    gl.glVertex3f(0,0,0);
    gl.glVertex3f(0,0,100);
    gl.glEnd();
    gl.glPopMatrix();
        
    }

    @Override
    public void init(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();
       // gl.glClearColor(0.8F, 0.8F, 0.8F, 1.0F);
        gl.glEnable(GL.GL_DEPTH_TEST);
        gl.glEnable(GL2.GL_LIGHTING);
        gl.glEnable(GL2.GL_LIGHT0);
        gl.glEnable(GL2.GL_COLOR_MATERIAL);
        gl.glEnable(GL2.GL_NORMALIZE);
    }

    @Override
    public void reshape(
            GLAutoDrawable arg0, int arg1, int arg2, int arg3, int arg4) {
    }

    @Override
    public void dispose(GLAutoDrawable arg0) {
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        lastX = e.getX();
        lastY = e.getY();
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        rotateX += e.getX() - lastX;
        rotateY += e.getY() - lastY;
        lastX = e.getX();
        lastY = e.getY();
    }

}